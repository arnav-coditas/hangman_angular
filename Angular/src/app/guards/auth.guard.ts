import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../Modules/auth/services/auth.service';

@Injectable({
  providedIn: 'any'
})
export class AuthGuard implements CanActivate {
  constructor(
    public router: Router,
    public authservice: AuthService,
   
  ) {}
  canActivate() {
    console.log('entering');
    if(this.authservice.LoggedIn()){
      console.log('entering in');
  
      return true;
    }


    // not logged in so redirect to login page with the return url
    
    return false;
  }
}
