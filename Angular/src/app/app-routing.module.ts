import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { RoleGuard } from './guards/role.guard';

const routes: Routes = [ {
  path: 'admin', loadChildren: ()=> import('./Modules/admin/admin.module').then(m=>m.AdminModule),
  canActivate:[AuthGuard,RoleGuard],data:{role:'admin'}
},

{
  path: '', loadChildren: ()=> import('./Modules/auth/auth.module').then(m=>m.AuthModule),
  // canActivate:[AuthGuard]
},
{
  path: 'player', loadChildren: ()=> import('./Modules/player/player.module').then(m=>m.PlayerModule),
  canActivate:[AuthGuard],data:{role:'player'}
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
