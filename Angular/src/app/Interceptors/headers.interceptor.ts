import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class HeadersInterceptor implements HttpInterceptor {
  #baseUrl=environment.base_url
token:any='';
// 

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler) {
    console.log(this.token)
    if(localStorage.getItem('token')){
     this.token=localStorage.getItem('token')
    }
 
    const newRequest=request.clone({
      url:this.#baseUrl+request.url,
     
       setHeaders: { Authorization: `Bearer ${this.token}` },
      
    })


    const nextReq=newRequest.clone({
      setHeaders: { 'ngrok-skip-browser-warning': '1234' },
      
    })
    
  
    return next.handle(nextReq);

    
  }



}
