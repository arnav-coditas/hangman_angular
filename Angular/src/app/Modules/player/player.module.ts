import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlayerRoutingModule } from './player-routing.module';
import { PlayerhomeComponent } from './components/playerhome/playerhome.component';
import { SharedModule } from '../shared/shared.module';
import { TournamentListComponent } from './components/playerhome/tournament-list/tournament-list.component';
import { ProfileComponent } from './components/profile/profile.component';
import{ MatTabsModule} from '@angular/material/tabs';
// import { AlltournamentsComponent } from './components/playerhome/tournament-list/alltournaments/alltournaments.component';
import { FormatTimePipe, GameComponent } from './components/playerhome/tournament-list/game/game.component'
import { MatButtonModule } from '@angular/material/button';
import { DetailsComponent } from './components/playerhome/tournament-list/alltournaments/details/details.component';
import { MatCardModule } from '@angular/material/card';
import { CreatetournamentComponent } from './components/playerhome/tournament-list/alltournaments/createtournament/createtournament.component';

import{HighchartsChartModule} from 'highcharts-angular'
import { MatDialog, MatDialogActions, MatDialogModule } from '@angular/material/dialog';

import { LeaderboardSectionComponent } from './components/profile/leaderboard-section/leaderboard-section.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HeadersInterceptor } from 'src/app/Interceptors/headers.interceptor';
import { PlayerLeaderBoardComponent } from './components/playerhome/tournament-list/game/player-leader-board/player-leader-board.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
@NgModule({
  declarations: [
    PlayerhomeComponent,
    TournamentListComponent,
    ProfileComponent,
    // AlltournamentsComponent,
    GameComponent,
    DetailsComponent,
    FormatTimePipe,
    CreatetournamentComponent,
    LeaderboardSectionComponent,
    PlayerLeaderBoardComponent
  ],
  imports: [
    CommonModule,
    PlayerRoutingModule,
    SharedModule,
    MatTabsModule,
    MatButtonModule,
    MatCardModule,
    HighchartsChartModule,
    MatDialogModule,
    HttpClientModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule

  
  ],
  providers: [MatDialog,{
    provide: HTTP_INTERCEPTORS,
    useClass: HeadersInterceptor,
    multi: true
}],
})
export class PlayerModule { }
