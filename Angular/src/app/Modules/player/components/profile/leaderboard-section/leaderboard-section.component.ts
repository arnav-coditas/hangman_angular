import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as Highcharts from 'highcharts';
import { PlayerService } from '../../../Services/player.service';


@Component({
  selector: 'app-leaderboard-section',
  templateUrl: './leaderboard-section.component.html',
  styleUrls: ['./leaderboard-section.component.scss']
})
export class LeaderboardSectionComponent implements OnInit {
  Highcharts: typeof Highcharts = Highcharts;
  message: string = ""
  cancelButtonText = "Cancel"
  constructor(
    private dataService:PlayerService,
    @Inject(MAT_DIALOG_DATA) private data: any,
  
    private dialogRef: MatDialogRef<LeaderboardSectionComponent>) {
    if (data) {
      this.message = data.message || this.message;
      if (data.buttonText) {
        this.cancelButtonText = data.buttonText.cancel || this.cancelButtonText;
      }
    }
    
    dialogRef.updateSize('300vw','300vw')
  }
  playerId:any
  ngOnInit(): void {
    if(localStorage.getItem('id')!==null){
      this.playerId= localStorage.getItem('id')
    }
   this.playerId= parseInt(this.playerId)
    // throw new Error('Method not implemented.');
    this.getGraph()
  }

  onConfirmClick(): void {
    this.dialogRef.close(true);
  }

  graphData:any
  chartOptions:any
  getGraph(){
    this.dataService.getPlayerProgressGraph(this.playerId).subscribe(data => {
      this.graphData = data;
      this.chartOptions = {
        title: {
          text: 'Tournaments played vs Time',
        },
        xAxis: {
          title: {
            text: 'Tournaments Played',
          },
          categories: this.graphData.tournamentName,
        },
        yAxis: {
          labels: {
            format: '{value:%M:%S}',
          },
          title: {
            text: 'Time Taken',
          },
        },
        series: [
          {
            name: 'John',
            data: this.graphData.gameDuration,
            type: 'line',
          },
        ],
      };
    });
  
  }
  



}
