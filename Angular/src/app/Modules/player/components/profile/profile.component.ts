import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { an } from 'chart.js/dist/chunks/helpers.core';
import { DialogComponent } from 'src/app/Modules/shared/Components/dialog/dialog.component';
import { PlayerService } from '../../Services/player.service';
import { UserlistService } from '../playerhome/tournament-list/alltournaments/userlist.service';
import { LeaderboardSectionComponent } from './leaderboard-section/leaderboard-section.component';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  title='Arnav'
   subTitle='Categories';
   caption='Ranks';
  cards=['card1'];
  tableData!: any[];
  dataSource!:any[]
  @Input() Event!:any[]
  playerInfo:any
  buttons=['Redeem'];
  id=localStorage.getItem('id')
  playerId:any
  constructor(private dialog: MatDialog,
    private formBuilder: FormBuilder,private playerservice:PlayerService,private route:Router,private snakbar:MatSnackBar
   ) {
    if(localStorage.getItem('id')!==null){
      this.playerId= localStorage.getItem('id')
    }
   this.playerId= parseInt(this.playerId)

    }




  ngOnInit(): void {
    this.createReset();
    this.viewProfileInfo()
    this.getGifts()
  }
  totalbalance!:number
viewProfileInfo(){

  this.playerservice.getProfileInfo(this.id).subscribe((res: any) => {
    this.playerInfo = res;
    this.totalbalance=res.totalCoins

    console.log(res);
  });

}

  columnHeader = {
    giftId: 'Gift Id',
    giftName: 'Gift Name',
    giftValue: 'Gift Price',
    // price:'price',
    actions: 'Actions',
  };
  name = 'World';
  redeemDetails!:any
  viewdetails(e: Event) {
    console.log(e);
    // this.play(e)
    this.redeemDetails = e;
    if (this.redeemDetails[0].details === 'Redeem') {

   this.redeemDetails[0].event.giftId
  const dialogRef = this.dialog.open(DialogComponent,{
    data:{
      description: 'Are you sure to Redeem?',
      
    
    }});
 
  dialogRef
    .afterClosed()
    .subscribe((data) => this.onRedeem(data));
}
  }

isOpen:boolean=false
toggle(){
  this.isOpen=!this.isOpen

  this.getGifts()
}
openDialog() {
  const dialogRef = this.dialog.open(LeaderboardSectionComponent,{
    data:{
      // message: 'Are you sure want to delete?',
      buttonText: {
        ok: 'Save',
        cancel: 'No'
      }
    }
  }); 
}

onRedeem(data:any){
let  redeemData={
    "playerId":this.playerId,
    "giftId":this.redeemDetails[0].event.giftId
  }
  
  this.playerservice.postRedeemRequest(redeemData).subscribe({
    next: (response: any) => {
    this.snakbar.open(`Redeem Request for ${this.redeemDetails[0].event.giftName} Successfull`)
      next: () => {};
    },
  
    complete: () => {
      setTimeout(() => {
        this.ngOnInit();
      }, 300);
      
    }
    
  });

}

resetDialog() {
  // console.log('hi');

  const dialogConfig = new MatDialogConfig();
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.data = {
    description: 'Reset Password',
    type: ['email', 'password', 'password'],
    placeHolder: ['userName', 'oldPassword', 'newPassword'],
    controls: ['userName', 'oldPassword', 'newPassword'],
    formgroup: this.resetGroup,
  };

  const dialogRef = this.dialog.open(DialogComponent, dialogConfig);
  dialogRef.afterClosed().subscribe((data) =>this.onReset(data));
}

resetGroup!: FormGroup;

createReset() {
  this.resetGroup = this.formBuilder.group({
    userName: ['', Validators.required],
    oldPassword: ['', Validators.required],
    newPassword: ['', Validators.required],
  });
}
giftsInfo:any
newArray:any
getGifts(){
  this.playerservice.getRedeemInfo().subscribe((res: any) => {
    // this.dataSource = res;
    console.log(res);
    this.newArray = res.filter( (el:any)=>
  {
    return this.totalbalance>=el.giftValue
  }
  );
this.dataSource=this.newArray
next:()=>{
  
}
  
// const results = res.filter((element:any) => {

//   return element.age === 30 && element.name === 'Carl';
// });


  });


}

onReset(data:any){
  this.playerservice.resetPassword(data).subscribe({
    next: (response: any) => {
      console.log('post Complete');
      next: () => {};
    },
  
    complete: () => {
      setTimeout(() => {
        this.ngOnInit();
      }, 300);
      
    }
    
  });

}
}

