import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { an } from 'chart.js/dist/chunks/helpers.core';
import { timer } from 'rxjs';
import { Subscription } from 'rxjs/internal/Subscription';
import { PlayerService } from 'src/app/Modules/player/Services/player.service';
import { DialogComponent } from 'src/app/Modules/shared/Components/dialog/dialog.component';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
})
export class GameComponent implements OnInit {
  constructor(private dialog: MatDialog, private formBuilder: FormBuilder,private route: ActivatedRoute,
    private router: Router,private playerData:PlayerService) {

      this.tournamentId =  this.route.snapshot.paramMap.get('id')
     this.tournamentId=parseInt(this.tournamentId)
      

    }
  countDown!: Subscription;
  counter = 0;
  tick = 1000;
  time!: number;
  visibility = 1;
  totalTime: any;
  score=0
  wordsData1:any;
  tournamentId!:any
  playerId!:any
  
  finalWords:any
  hintmessage!:[]
incrementHint=0;


  ngOnInit(): void {
    this.getGameWords()
    this.startTimer();
    if(localStorage.getItem('id')!==null){
      this.playerId= localStorage.getItem('id')
    }
   this.playerId= parseInt(this.playerId)

  
  

    // this.forgot()
    // this.tournamentId =  this.route.snapshot.paramMap.get('id')
    // console.log(this.tournamentId)
    // this.tournamentId.parseInt


    console.log(this.words[this.words.length - 1]);
    if (this.words[this.words.length - 1] === this.words[this.i]) {
      this.counter = 0;
    }
   
  }
  

  // forgotGroup!: FormGroup;
  openDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
      description: this.counter,
      // caption: this.score,
      // type: ['userNameOrEmail'],
      // placeHolder: ['Email'],
      // controls: ['userNameOrEmail'],
      // formgroup: this.forgotGroup,
    };
    
    const dialogRef = this.dialog.open(DialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((data) =>this.postGameData(data) ,
  
    // this.router.navigate('/'));
    )
  }
  // forgot() {
  //   this.forgotGroup = this.formBuilder.group({
  //     userNameOrEmail: ['', Validators.required],
  //   });
  // }

  i = 0;
  

  game_start(): void {
    this.visibility = 0;

    // this.startTimer()
    if (this.started == false) {
      this.life = 5;
      //  let i=this.i

      for (let z = 0; z < this.words[this.i].length; z++) {
        this.chosenWord[z] = this.words[this.i].charAt(z);
        this.wordBottom[z] = '_';
      }
      this.atualLength = 0;
      this.wordLength = this.words[this.i].length;
      this.started = true;

      this.message = '';
    } else {
      for (let c = 0; c < 26; c++) {
        document.getElementById(c.toString())!.hidden = false;
      }
      for (let z = 0; z < this.chosenWord.length; z++) {
        this.chosenWord[z] = '';
        this.wordBottom[z] = '';
      }
      for (let y = 0; y < 10; y++) {
        console.log('y is ', y);
        document.getElementById('l_' + y.toString())!.style.visibility =
          'hidden';
      }

      // let i = 0;
      for (let z = 0; z < this.words[this.i].length; z++) {
        this.chosenWord[z] = this.words[this.i].charAt(z);
        this.wordBottom[z] = '_';
        // this.i++
      }

      this.atualLength = 0;
      this.wordLength = this.words[this.i].length;
      this.life = 5;
      this.finished = false;
      this.started = true;
      this.message = '';
    }
  }
  title = 'Hangman';
  message = '';

  started = false;
  finished = false;

  life = 5;
  wordLength = 100;
  atualLength = 0;
  chosenWord: string[] = ['', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', ''];
  wordBottom: string[] = ['', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', ''];
  img_link: string[] = [
  ];

  startTimer() {
    this.countDown = timer(0, this.tick).subscribe((count) => {
      if (this.counter == 0 && count) {
        console.log('timer completed', count);
        this.totalTime = count;
        if (this.countDown) {
          this.countDown.unsubscribe();
        }
      }
      ++this.counter;
    });
  }

words:any
  a = 0;
  choice(z: any, id: any): void {
    console.log(z);
    if (this.started == true && this.finished == false) {
      let fail = true;
      for (let j = 0; j < 10; j++) {
        if (this.chosenWord[j] == z) {
          document.getElementById('l_' + j.toString())!.style.visibility =
            'visible';
          this.atualLength += 1;
          fail = false;
        }
      }

      if (fail == true) {
        this.life -= 1;
        console.log(this.life);
      }

      document.getElementById(id)!.hidden = true;

      if (this.life <= 0) {
        if (this.words[this.words.length - 1] === this.words[this.i]) {
          console.log(this.counter);
          this.totalTime = this.counter;

          this.openDialog();
          this.counter = 0;
        }
        this.finished = false;

        setTimeout(() => {
          this.incrementHint++
          this.message = 'YOU LOOSE';
          this.hintmessage=this.hints[this.incrementHint]
        }, 100);

        this.i++;
      
        this.game_start();
      }
      if (this.atualLength >= this.wordLength) {
        setTimeout(() => {
          this.message = 'YOU WIN';
          this.incrementHint++
          this.hintmessage=this.hints[this.incrementHint]
          this.score++;
        
          console.log(this.score)
        }, 100);
        if (this.words[this.words.length - 1] === this.words[this.i]) {
          this.totalTime = this.counter;

          this.openDialog();
          this.counter = 0;
        }
        this.finished = false;



      
        this.i++;
       
        this.game_start();
      }
    }
  }
allWords:any
onlyWords:any
hints:any
wordCount!:number
  getGameWords(){
    console.log(this.tournamentId)
    
    this.playerData.getGameQuestionSet(this.tournamentId).subscribe({
      
      next: (response: any) => {
        this.allWords = response;
        console.log(this.allWords);
        
        this.onlyWords = this.extractwordValue(this.allWords, 'wordName');
        this.hints=this.extractwordValue(this.allWords, 'hintName')
        console.log(this.hints)
       
        this.onlyWords = this.onlyWords.map((element:any) => {
            return element.toLowerCase();
        });

        this.wordCount=this.onlyWords.length
        console.log(this.wordCount)
        console.log(this.onlyWords)
       this.finalWords=this.onlyWords
       this.words=this.onlyWords
       this.hintmessage=this.hints[0]
        next: () => {
        };
      },

      complete: () => {
      },
    });
  }



 extractwordValue(arr:any, prop:any) {
    // extract value from property
    let extractedValue = arr.map((item:any) => item[prop]);
    return extractedValue;
}

finalScoreToBeSent!:number


postGameData(data:string){

let scoreToBeSent= this.score*100/this.wordCount;
this.finalScoreToBeSent=parseInt(scoreToBeSent.toFixed(2))
// scoreToBeSent=scoreToBeSent.toFixed(2)
 if(data==='true'){
let gameFinishedData={
  "gameDuration":this.totalTime*1000,
  "score":this.finalScoreToBeSent,
  "tournamentId":this.tournamentId,
  "playerId":this.playerId

}
console.log(gameFinishedData)
  

this.playerData.postScoreToLeaderBoard(gameFinishedData).subscribe({
  next: (response: any) => {
    console.log('post Complete');
    next: () => {};
  },

  complete: () => {
    setTimeout(() => {
      this.ngOnInit();
    }, 300);
    this.router.navigate(['/player/game/leaderboard',{id:this.tournamentId}])
  },
});



 }
}
}



@Pipe({
  name: 'formatTime',
})
export class FormatTimePipe implements PipeTransform {
  transform(value: number): string {
    const minutes: number = Math.floor(value / 60);
    return (
      ('00' + minutes).slice(-2) +
      ':' +
      ('00' + Math.floor(value - minutes * 60)).slice(-2)
    );
  }
}
