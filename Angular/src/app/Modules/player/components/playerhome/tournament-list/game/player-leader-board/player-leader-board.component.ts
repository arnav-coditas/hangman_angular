import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PlayerService } from 'src/app/Modules/player/Services/player.service';

@Component({
  selector: 'app-player-leader-board',
  templateUrl: './player-leader-board.component.html',
  styleUrls: ['./player-leader-board.component.scss']
})
export class PlayerLeaderBoardComponent implements OnInit {


  
  buttons = ['Play', 'View'];
  title = 'Arnav';
  subTitle = 'Categories';
  caption = 'Ranks';
  cards = ['card1'];
  leaderBoardData:any
  constructor(private  playerData:PlayerService,private route: ActivatedRoute,private router:Router) {

    
    this.tournamentId =  this.route.snapshot.paramMap.get('id')
    this.tournamentId=parseInt(this.tournamentId)
   }
tournamentId:any
  ngOnInit(): void {
    this.getLeaderBoard()
  }


  getLeaderBoard(){
 this.playerData.getLeaderBoardAfterGameEnds(this.tournamentId).subscribe((res: any) => {
  this.leaderBoardData = res;
  console.log(this.leaderBoardData);
});


}
navigate(){
  this.router.navigate(['/player/list'])
}
}