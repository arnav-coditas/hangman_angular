import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerLeaderBoardComponent } from './player-leader-board.component';

describe('PlayerLeaderBoardComponent', () => {
  let component: PlayerLeaderBoardComponent;
  let fixture: ComponentFixture<PlayerLeaderBoardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlayerLeaderBoardComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PlayerLeaderBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
