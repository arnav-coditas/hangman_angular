import { Component, Input, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  Route,
  Router,
} from '@angular/router';
import { CreateTournamentDialogComponent } from 'src/app/Modules/shared/Components/create-tournament-dialog/create-tournament-dialog.component';
import { DialogComponent } from 'src/app/Modules/shared/Components/dialog/dialog.component';
import { PlayerService } from '../../../Services/player.service';
import { CreatetournamentComponent } from './alltournaments/createtournament/createtournament.component';
import { UserlistService } from './alltournaments/userlist.service';

@Component({
  selector: 'app-tournament-list',
  templateUrl: './tournament-list.component.html',
  styleUrls: ['./tournament-list.component.scss'],
})
export class TournamentListComponent implements OnInit {
  tableData: any;
  dataSource: any;
  @Input() Event!: any[];
  specificPlayedData: any;
  id: any;
  categoryValues: string='default'
  pageSizeValue: any;
  constructor(
    public bookListService: UserlistService,
    public router: Router,
    public dialog: MatDialog,
    public playerservice: PlayerService,
    private route: ActivatedRoute,
    private snakbar : MatSnackBar
  ) {
    this.id = localStorage.getItem('id');
    if (localStorage.getItem('id') !== null) {
      this.id = localStorage.getItem('id');
      this.id = parseInt(this.id);
    }
    this.getMyTournamentsList();
    this.getPlayerData()
  }

  buttons = ['Play'];
  title = 'Arnav';
  subTitle = 'Categories';
  caption = 'Ranks';
  cards = ['card1'];
  items = ['item', 'item2', 'item3', 'item4'];

  ngOnInit(): void {
    // this.getavailabletournaments();
    this.getPlayedTournamentList();

    this.getMyTournamentsList();
  

    this.getCategories()
  }
  tournamentId!: number;

  playerData: any;
  viewdetails(e: Event) {
    console.log(e);

    this.playerData = e;
    this.tournamentId = this.playerData[0].event.tournamentId;

    this.playerData = e;
    // this.tournamentId=e.tournamentId
    if (this.playerData[0].details === 'Play') {
      this.router.navigate(['player/list/details', { id: this.tournamentId }]);
    }
    // else if(this.playerData[0].details === 'Stop'){

    // }
  }

  columnHeader = {
    tournamentId: 'ID',
    tournamentName: 'Tournament Name',
    firstPriceCoins: 'firstPriceCoins',
    secondPriceCoins: 'secondPriceCoins',
    thirdPriceCoins: 'thirdPriceCoins',
    actions: 'Actions',
  };
  name = 'World';

  createTournament() {
    const dialogConfig = new MatDialogConfig();
    const dialogRef = this.dialog.open(
      CreateTournamentDialogComponent,
      dialogConfig
    );
    dialogRef.afterClosed().subscribe((data) => this.onCreateTournament(data));
  }

  callBack(name: string) {
    this.name = name;
  }

  onCreateTournament(data: any) {
    console.log(data);
    console.log(this.id);

    this.playerservice.postCreateTouraments(data, this.id).subscribe({
      next: (response: any) => {
        console.log('Create Complete');
        next: () => {};
      },

      complete: () => {
        setTimeout(() => {
          this.ngOnInit();
        }, 300);
      },
    });
  }
data=""
empfilters:any
 

  categories:any

  getCategories(){
    this.playerservice.getCategoryDetails().subscribe((res: any) => {
     this.categories= res;
      console.log(res);
      }  )
  
  }
  filtervalue:string[]=[]

page=['5','10','15','20']
sortBy=['asc','desc']
order:string[]=['desc']
pageNo:number=3

  getPlayerData(){
    let url = `categoryValues=${this.categoryValues}&pageSize=${this.pageNo}&sortBy=tournamentName&pageNo=0&sortDir=${this.order}`
  
    console.log(url)
    this.playerservice.getavailabletournaments(url,this.id).subscribe((res: any) => {
      this.dataSource = res;
      console.log(res)
     
      console.log(url)
    })
  }
  filterByCategory(value:string[]){
    this.categoryValues = value.join(',')
    this.getPlayerData()
  }

  filterByPage(value:number){
    console.log(value)
    this.pageNo=value
    console.log(value)
    this.getPlayerData()
  }
filterBySort(value:string[]){
console.log(value)
this.order=value
this.getPlayerData()

}
 
  

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    // this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  

  myTournamentData: any;

  getMyTournamentsList() {
    this.playerservice.getMyTournaments(this.id).subscribe((res: any) => {
    this.myTournamentData = res;
      console.log(res);
      // this.myTournamentData = res;
    });
  }

  stoptournament(id:number){

   let  sendStopDetails={
    
    "tournamentId" :id,
    "playerId":this.id
   }
   this.playerservice.stopTournament(sendStopDetails).subscribe({
    next: (response: any) => {
      this.snakbar.open("Tournament Stopped")
      next: () => {};
    },

    complete: () => {
      setTimeout(() => {
        this.ngOnInit();
      }, 300);
    },
  });



  }

  getPlayedTournamentList() {
    this.playerservice
      .getSpecificPlayedTournaments(this.id)
      .subscribe((res: any) => {
        // this.dataSource = res;
        console.log(res);
        this.specificPlayedData = res;
      });
  }
}
