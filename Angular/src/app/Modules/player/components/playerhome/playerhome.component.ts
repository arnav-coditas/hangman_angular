import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PlayerService } from '../../Services/player.service';

@Component({
  selector: 'app-playerhome',
  templateUrl: './playerhome.component.html',
  styleUrls: ['./playerhome.component.scss']
})
export class PlayerhomeComponent implements OnInit {

  selectedItem!:string

 title= ['Player']
navtitle='Player'
     items = [
        {
          tabs :'Tournaments List',
          icons: 'supervised_user_circle',
        },
        {
          tabs: 'Profile',
          icons: 'speaker_notes',
        },
        {
          tabs :'Points',
          icons: 'list_alt',
        },
        {
          tabs: 'Logout',
          icons: 'toc',
        },
      ];
 id:any
  constructor(private router:Router,private service:PlayerService) {

    this.id = localStorage.getItem('id');
    if (localStorage.getItem('id') !== null) {
      this.id = localStorage.getItem('id');
      this.id = parseInt(this.id);
    }
   }
  currentTimeInSeconds!:number
  ngOnInit(): void {
}
  onSelectedItem(item:string){
    this.selectedItem=item  
  console.log(this.selectedItem)
  if (this.selectedItem==='Tournaments List'){
    this.router.navigate(['/player/list'])
  }
  else if(this.selectedItem==='Profile'){
    this.router.navigate(['/player/profile/'])
  }
  else if(this.selectedItem==='Logout'){
    this.currentTimeInSeconds=Math.floor(Date.now()/1000)
    console.log(this.currentTimeInSeconds)
let timeDate={
  "playerId":this.id,
  "duration":this.currentTimeInSeconds

}

   this.service.sendTimeDurantion(timeDate).subscribe((res) => {
  
      // this.myTournamentData = res;
    });
   console.log("Logged Out Successfully")
    this.router.navigate([''])
  }
  
  
}




}

