import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlayerhomeComponent } from './components/playerhome/playerhome.component';
// import { AlltournamentsComponent } from './components/playerhome/tournament-list/alltournaments/alltournaments.component';
import { DetailsComponent } from './components/playerhome/tournament-list/alltournaments/details/details.component';
import { GameComponent } from './components/playerhome/tournament-list/game/game.component';
import { PlayerLeaderBoardComponent } from './components/playerhome/tournament-list/game/player-leader-board/player-leader-board.component';
import { TournamentListComponent } from './components/playerhome/tournament-list/tournament-list.component';
import { ProfileComponent } from './components/profile/profile.component';

const routes: Routes = [
  {
    path: '',
    component: PlayerhomeComponent,
    children: [
      {
        path: 'list',
        component: TournamentListComponent,

        children: [
         
          {
            path: 'details',
            component: DetailsComponent,
          },


        ],


      },

      {
        path: 'profile',
        component: ProfileComponent,
      },


      {
        path: 'game',
        component: GameComponent,
        children: [
         
          {
            path: 'leaderboard',
            component: PlayerLeaderBoardComponent,
          },


        ],
        
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],

})
export class PlayerRoutingModule {}
