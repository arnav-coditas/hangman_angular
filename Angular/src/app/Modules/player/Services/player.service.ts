import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'any'
})
export class PlayerService {

  constructor(private http:HttpClient) { }

  getAllTournaments(Id:any,data:any){
    
    return this.http.get(`/api/player/availableTournaments/${Id}`)
    
   

  }
  getavailabletournaments(url:any,Id:number){
    if(url!==null){
    return this.http.get(`/getAllTournaments?${url}`)
    }else{
      console.log(Id)
      return this.http.get(`/api/player/availableTournaments/${Id}`)
    }
  }

  getProfileInfo(Id:any){
    return this.http.get(`/api/player/getSelfInfo/${Id}`)
  }

  getRedeemInfo(){
    return this.http.get(`/gifts/getGiftList`)
  }

  postCreateTouraments(data:any,Id:any){
    console.log(data)
return this.http.post(`/api/player/createTournament/${Id}`,data)
  }

  getGameQuestionSet(Id:number){
    return this.http.get(`/questionSet/${Id}`)
  }


  postScoreToLeaderBoard(data:any){
    return this.http.post('/api/addLeadBoard',data)
  }


  getMyTournaments(Id:number){
    
    return this.http.get(`/api/player/selfCreated/${Id}`)
  }


  getSpecificPlayedTournaments(Id:number){
    return this.http.get(`/api/player/getLeadBoardForParticularPlayer/${Id}`)
  }

getLeaderBoardAfterGameEnds(Id:number){
  return this.http.get(`/api/getScoresForTournament/${Id}`)
}

getPlayerProgressGraph(Id:number){
  return this.http.get(`/api/player/graph/${Id}`)
}

resetPassword(data:any){
  return this.http.post(`/api/reset`,data)

}

postRedeemRequest(data:any){
  console.log(data)
  return this.http.post(`/request/addGiftRequest`,data)
}


getCategoryDetails() {
  return this.http.get('/getCategoryList');
}

stopTournament(data:any){
  return this.http.post(`/api/player/stopTournament`,data)
}

sendTimeDurantion(data:any){
  return this.http.post('/api/player/logout',data)
}
}
