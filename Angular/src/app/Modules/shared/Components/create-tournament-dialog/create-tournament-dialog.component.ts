import { Component, Inject, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Data } from '@angular/router';
import { AdminService } from 'src/app/Modules/admin/Services/admin.service';
import { DialogComponent } from '../dialog/dialog.component';
export type DialogDataSubmitCallback<T> = (row: T) => void;
@Component({
  selector: 'app-create-tournament-dialog',
  templateUrl: './create-tournament-dialog.component.html',
  styleUrls: ['./create-tournament-dialog.component.scss']
})
export class CreateTournamentDialogComponent implements OnInit {
    
  categoryList :any

  input: FormControl = new FormControl('');


  form: FormGroup = new FormGroup({
  
  });
  createForm!:FormGroup;
  

  constructor(
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA)
    public data: { callback: DialogDataSubmitCallback<any>; defaultValue: any },
    private dialogRef: MatDialogRef<DialogComponent>,
    private fb:FormBuilder,
    private dataList:AdminService
  ) {

    this.createTournament = this.fb.group({
      tournament: this.fb.array([])
    });
  }
  ngOnInit(): void {
    this.getCategory()

  }
  createTournament!: FormGroup;

   
  

  employees(): FormArray {
    return this.createTournament.get('tournament') as FormArray;
  }

  newEmployee(): FormGroup {
    return this.fb.group({
      tournamentName: ['' ],
      firstPriceCoins: '',
      secondPriceCoins: '' ,
      thirdPriceCoins: '',
      categoryList: ['', [Validators.required]],
      wordDtoList: this.fb.array([])
    });
  }
 
visibility=1
  addEmployee() {
    console.log('Adding an employee');
    this.employees().push(this.newEmployee());
    this.visibility=0
  }

  removeEmployee(empIndex: number) {
    this.employees().removeAt(empIndex);
  }

  employeeSkills(empIndex: number): FormArray {
    return this.employees()
      .at(empIndex)
      .get('wordDtoList') as FormArray;
  }

  newSkill(): FormGroup {
    return this.fb.group({
      wordName: '',
      hintName: ''
    });
  }

  addEmployeeSkill(empIndex: number) {
    this.employeeSkills(empIndex).push(this.newSkill());
  }

  removeEmployeeSkill(empIndex: number, skillIndex: number) {
    this.employeeSkills(empIndex).removeAt(skillIndex);
  }

  onSubmit() {
    console.log(this.createTournament.value);
  }
  createTourametData!:any

  save() {
 
   
    console.log(this.createTournament.value)
    this.createTourametData=this.createTournament.value
console.log(this.createTourametData.tournament[0])
this.dialogRef.close(this.createTourametData.tournament[0]);
 }
 close(){
  this.dialogRef.close()
  console.log("only closed")
}


getCategory(){
  this.dataList.getCategoryDetails().subscribe((res: any) => {
  this.categoryList=res
    console.log(res);
  })
}


}

















    

  //   this.form = this.fb.group({
  //     website: ['', [Validators.required]],
  //     name: ['', [Validators.required]],
  //     phones: this.fb.array([
  //       this.fb.control(null)]),
  //     Values: this.fb.array([
  //     this.fb.control(null)
  
  //     ])
    

  //   })


 
  // }
  // ngOnInit(): void {
  //   throw new Error('Method not implemented.');
  // }
  
  // // ngOnInit() {
  // //   if (this.data.defaultValue) {
  // //     this.input.patchValue(this.data.defaultValue);
  // //   }
    
   

  // //   // this.createForm = this.formBuilder.group({
  // //   //   nurseName: ['', Validators.required],
  // //   //   email: ['', Validators.required],
  // //   // });

  // //   }
   

  //   addPhone(): void {
  //     (this.form.get('phones') as FormArray).push(
  //       this.fb.control(null)
  //     );
  //     (this.form.get('values') as FormArray).push(
  //       this.fb.control(null)
  //     );
  //   }

  // get f(){
 
  //   return this.form.controls;
  // }
    
  // submit(){
  //   this.dialogRef.close(this.form.value);
  //   console.log(this.form.value);
  //   console.log('sumbitted')
  // }

  // getValuesfromControls():any{
  //   return(
  //     (<FormArray> this.form.get('Values')).controls
  //   )
  // }
  // getPhonesFormControls():any {

  //   return(
  //    (<FormArray> this.form.get('phones')).controls
  //   )
  // }
  // send(values:any) {
  //   console.log(values);
  // }
  // }
  

  

  

// createForm!: FormGroup;
//   empForm: any;

//   constructor(private fb: FormBuilder) {}

//   ngOnInit() {



//     this.createForm = this.fb.group({
//       employees: this.fb.array([])
//     });
//   }

//   employees(): FormArray {
//     return this.empForm.get('employees') as FormArray;
//   }

//   newEmployee(): FormGroup {
//     return this.fb.group({




      
//       firstName:'',
//       skills: this.fb.array([]),
      
//     });
//   }

//   addEmployee() {
//     this.employees().push(this.newEmployee());
//   }

//   removeEmployee(empIndex: number) {
//     this.employees().removeAt(empIndex);
//   }

//   employeeSkills(empIndex: number): FormArray {
//     return this.employees()
//       .at(empIndex)
//       .get('skills') as FormArray;
//   }

//   newSkill(): FormGroup {
//     return this.fb.group({
    
//       skill: '',
//       skill2:'',
//       skill3:''
   
//     });
//   }

//   addEmployeeSkill(empIndex: number) {
    
//     this.employeeSkills(empIndex).push(this.newSkill());


//   }

//   expression='visible'
//   // removeEmployeeSkill(empIndex: number, skillIndex: number) {
//   //   this.employeeSkills(empIndex).removeAt(skillIndex);
//   // }

//   onSubmit() {
//     console.log(this.empForm.value);

//   }



  
