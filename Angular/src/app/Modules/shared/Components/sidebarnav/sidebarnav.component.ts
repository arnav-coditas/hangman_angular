import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-sidebarnav',
  templateUrl: './sidebarnav.component.html',
  styleUrls: ['./sidebarnav.component.scss']
})
export class SidebarnavComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  @Input() items!: any;
  @Input() title!: string[];
  @Input() icons!:string[];
  @Output() selected = new EventEmitter();
  activeItem = 0;



  onSelect(item: string, index: number) {
    this.activeItem = index;
    this.selected.emit(item);
  }

}
