import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }

  // @Input() cards!:any;
  @Input() title!: string;
  @Input() subTitle!: string;
  @Input() caption!: string;
  @Input() avtar!: string;
  @Input() image!: string;
  @Input()  cards!:any;
  @Output() likeNotify = new EventEmitter<boolean>();
  @Output() shareNotify = new EventEmitter<boolean>();
  @Output() learnMoreNotify = new EventEmitter<boolean>();













  // likeEvent() {
  //   this.likeNotify.emit(true);
  // }
  // shareEvent() {
  //   this.shareNotify.emit(true);
  // }
 buttonEvent() {
    this.learnMoreNotify.emit(true);
  }
}
