import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
@Input() buttons:any;
  // buttons=['Play','View']
  @Input() tabledata: any;
  @Input() columnHeader!:any;
  @Input() service!:any;
  @Output()  clickEvent = new EventEmitter()
  @Output()  eventType= new EventEmitter()
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  objectKeys = Object.keys;
  @Input() dataSource!: any;
  isLoadingResults = false;
  resultsLength!:any;
  details: any;

  constructor() {}

  ngOnInit() {
   
    this.dataSource = new MatTableDataSource(this.tabledata);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  buttonEvent!:any
  handleClick(event:any, eventType:any) {
    console.log(this.service);
    console.log(event, eventType);
    // this.buttonEvent[0]=event
    this.buttonEvent=event

     
    let  data=[{
        event:event,
        details:eventType
      }]
      this.clickEvent.emit(data)
  }
 


}
