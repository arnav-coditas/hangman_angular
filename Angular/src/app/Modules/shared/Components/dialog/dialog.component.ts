import { outputAst } from '@angular/compiler';
import { Component, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})

export class DialogComponent implements OnInit {

  form!: FormGroup;
  description:any;
  Value:any
type!:any
  formGroup!:any;
  caption!:number
  // forgotGroup!:FormGroup;
  constructor(
      private fb: FormBuilder,private dialogRef: MatDialogRef<DialogComponent>,@Inject(MAT_DIALOG_DATA) public data:any) {
     
      this.data=data
      this.form=data.formgroup;

                    // this.Value=data.value
                   
       this.description = data.description;
     
        this.caption=data.caption
      
  }
  // ngOnChanges(changes: SimpleChanges): void {
  //   this.form=this.data.formgroup
  //     //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
  //   //Add '${implements OnChanges}' to the class.
    
  // }
  

  ngOnInit() {

  }

  save() {
 
     this.dialogRef.close(this.form.value);
     
     console.log(this.form.value)
     console.log(this.form)
     
  }

close(){
  this.dialogRef.close()
  console.log("only closed")
}

}

  


