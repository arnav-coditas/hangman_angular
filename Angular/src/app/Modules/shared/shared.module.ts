import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import { SidebarnavComponent } from './Components/sidebarnav/sidebarnav.component';
import { MatSidenavModule, } from "@angular/material/sidenav";
import {MatListModule,} from "@angular/material/list";
import {  MatIconModule,} from "@angular/material/icon";
import { MatCardModule } from "@angular/material/card";
import { MatButtonModule } from "@angular/material/button";


import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavbarComponent } from './Components/navbar/navbar.component';
import{ MatToolbarModule} from '@angular/material/toolbar';
import { TableComponent } from './Components/table/table.component'
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { CardComponent } from './Components/card/card.component';
import { DialogComponent } from './Components/dialog/dialog.component';
import { MatDialogActions, MatDialogModule } from '@angular/material/dialog';
import { CreateTournamentDialogComponent } from './Components/create-tournament-dialog/create-tournament-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatOptionModule, MatPseudoCheckboxModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';


@NgModule({
  declarations: [
    SidebarnavComponent,
    NavbarComponent,
    TableComponent,
    CardComponent,
    DialogComponent,
    CreateTournamentDialogComponent
  ],
  imports: [
    CommonModule,
    SharedRoutingModule,
    MatCardModule,
    MatButtonModule, 
  
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    MatToolbarModule,
    MatPaginatorModule,
    MatTableModule,
    MatFormFieldModule,
    MatDialogModule,
    ReactiveFormsModule,
FormsModule,
MatInputModule,
MatPseudoCheckboxModule,
MatOptionModule,
MatSelectModule,
MatButtonModule
  
  
  ],
  exports:[SidebarnavComponent,NavbarComponent,TableComponent,CardComponent,CreateTournamentDialogComponent]


})
export class SharedModule { }
