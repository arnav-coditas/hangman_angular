import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';

import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import {MatFormFieldControl, MatFormFieldModule, MatLabel, MAT_ERROR, MAT_FORM_FIELD_DEFAULT_OPTIONS} from '@angular/material/form-field'
import {MatInputModule} from '@angular/material/input'
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { HttpClientModule, HttpClientXsrfModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HeadersInterceptor } from 'src/app/Interceptors/headers.interceptor';
import {MatSnackBarModule} from '@angular/material/snack-bar';
@NgModule({
  declarations: [
    
    LoginComponent,
   SignupComponent
    
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatDialogModule,
    HttpClientModule,
    MatSnackBarModule,
    HttpClientXsrfModule,
   
    
  ],
  providers: [MatDialog,{
    provide: HTTP_INTERCEPTORS,
    useClass: HeadersInterceptor,
    multi: true
}],
})
export class AuthModule { }
