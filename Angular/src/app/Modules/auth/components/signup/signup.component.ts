import { Component, forwardRef, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  Validators,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { IPost } from '../../Interfaces/ipost';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],

  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SignupComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => SignupComponent),
      multi: true,
    },
  ],
})
export class SignupComponent implements OnInit {
  formGroup!: FormGroup;
  constructor(private formBuilder: FormBuilder,private auth: AuthService,private router:Router,private snakbar:MatSnackBar) // private dialog: MatDialog,
  {}

  ngOnInit(): void {
    this.createSignUpForm();
  }

  signUpData!:IPost;
  getvalue() {
    this.signUpData = this.formGroup.value;
    console.log(this.signUpData);

    this.auth.postSignUpDetails(this.signUpData).subscribe({
      next: (response: any) => {
        console.log(response);

        next: () => {};
      },

      complete: () => {
        console.log('DONE');
        this.snakbar.open("Account Created Successfully!")
        this.router.navigate([''])
        
      },
    });
    
    
  }

  createSignUpForm() {
    this.formGroup = this.formBuilder.group({
      userName: ['', Validators.required],
      userPassword: ['', Validators.required],
      playerName: ['', Validators.required],
    
    });
  }

  onSignUpData(){
 
  
        // this.token.Token = this.tokendata;
     
      
        // if (this.token.role === 'admin') {
        //   this.router.navigate(['admin']);
        // } else if (this.token.role === 'doctor') {
        //   this.router.navigate(['doctor']);
       
        // } else {
        //   console.log('INVALID CREDENTIALS!');
        // }
        
      }
//     });
//   }
// }
    }