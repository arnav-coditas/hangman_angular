import { Component, forwardRef, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  Validators,
} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatDialogConfig } from '@angular/material/dialog';
import { Router, RouterModule } from '@angular/router';
import { an } from 'chart.js/dist/chunks/helpers.core';
import { DialogComponent } from 'src/app/Modules/shared/Components/dialog/dialog.component';
import { IPost } from '../../Interfaces/ipost';
import { AuthService } from '../../services/auth.service';
import {MatSnackBar, MatSnackBarModule} from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => LoginComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => LoginComponent),
      multi: true,
    }
  ],
})
export class LoginComponent implements OnInit {
formGroup!:FormGroup
  constructor(
    private formBuilder: FormBuilder,
  private dialog: MatDialog,
  private auth:AuthService,
  private route:Router,
  private snakbar : MatSnackBar

  ) { }

  ngOnInit(): void {
    this.createLoginForm();
    this.forgot();
    this.createOTPForm()
  }

  forgot() {
    this.forgotGroup = this.formBuilder.group({
      userName: ['', Validators.required],
    });
  }

onSubmit(data:object){
console.log(data)
}

  createLoginForm() {
    this.formGroup = this.formBuilder.group({
      userName: ['', Validators.required,Validators.email],
      password: ['', Validators.required],
     
      
  
    });  
  }

  createOTPForm(){
    this.otpGroup = this.formBuilder.group({
      email: ['', Validators.required],
      otp: ['', Validators.required],
      newPassWord: ['', Validators.required],
     
      
  
    });  
    
  }

 

  forgotGroup!: FormGroup;
  openDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
      description: 'Forgot Password',
      type: ['userName'],
      placeHolder: ['Email'],
      controls: ['userName'],
      formgroup: this.forgotGroup
      
    };
    const dialogRef = this.dialog.open(DialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((data) =>this.onForget(data));
    // dialogRef.backgroundolor('300vw','300vw')


    
  }
otpGroup!:FormGroup
  
onForget(data:any){
  this.otpStart()
this.auth.postForgotDetails(data).subscribe({
  next: (response: any) => {
    console.log(response);
       this.snakbar.open("OTP Sent Successfully!")
}})

    
  } 








otpStart(){
  const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
      description: 'Verify OTP',
      type: ['Email','text','password'],
      placeHolder: ['email','otp','newPassWord'],
      controls: ['email','otp','newPassWord'],
      formgroup: this.otpGroup,
      
    };
    const dialogRef = this.dialog.open(DialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((data) =>this.onOTP(data));


}







  onOTP(data:any){
    this.auth.postOTPDetails(data).subscribe({
      next: (response: any) => {
        console.log(response);
 this.snakbar.open("Password Updated Successfully")
        
  }})
  }
    
  
  // forgotPass(data: any) {
  //   this.auth.forgotpass(data).subscribe({
  //     next: (response: any) => {
  //       console.log('Password Sent');

  //       next: () => {};
  //     },

  //     complete: () => {
  //       setTimeout(() => {
  //         this.ngOnInit();
  //       }, 300);
  //     },
  //   });
  // }
// data!:object
//   getvalue() {
//     this.data = this.formGroup.value;
//     console.log(this.data)
//   }
  // sendLoginDetails(data: any) {
  //   this.auth.postLoginDetails(this.data).subscribe({
  //     next: (response: any) => {
  //       console.log(response);
  //       this.tokendata = response;
  //       // this.token.Token = this.tokendata;
  //       this.token.setToken(this.tokendata);
  //       this.token.role = this.tokendata.tokenRole;
  //       console.log(this.token.role);
  //       if (this.token.role === 'admin') {
  //         this.router.navigate(['admin']);
  //       } else if (this.token.role === 'doctor') {
  //         this.router.navigate(['doctor']);
  //       } else if (this.token.role === 'nurse') {
  //         this.router.navigate(['nurse']);
  //       } else {
  //         console.log('INVALID CREDENTIALS!');
  //       }
        
  //     }
  //   });
  // }

  

 
   loginData!:IPost
  getvalue() {
    this.loginData = this.formGroup.value;
 console.log(this.loginData)
  
    this.auth.postLoginDetails(this.loginData).subscribe({
      next: (response: any) => {
        console.log(response);
        // this.snakbar.open("Successfully Logged In!")
localStorage.setItem('token',response.accessToken)
localStorage.setItem('tokenRole',response.tokenRole)
localStorage.setItem('id',response.tokenUserID)
if (response.tokenRole === 'player') {

  
    this.snakbar.open("Successfully Logged In as Player")


  this.route.navigate(['player']);

} else if (response.tokenRole  === 'admin') {
  this.route.navigate(['admin']);
    this.snakbar.open("Successfully Logged In as Admin")
  
}  else {
  this.snakbar.open("Invalid Credentials!")
  
}
      },
error:()=>{
  this.snakbar.open("Invalid Credentials!")
},
      complete: () => {
        console.log('DONE');
      },
    });
    
  }

  
}

