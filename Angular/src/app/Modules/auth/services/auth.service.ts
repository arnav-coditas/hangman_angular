import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { an } from 'chart.js/dist/chunks/helpers.core';
import { environment } from 'src/environments/environment';
import { IPost } from '../Interfaces/ipost';

@Injectable({
  providedIn: 'any',
})
export class AuthService {
  // headers = new HttpHeaders().set('ngrok-skip-browser-warning', '1234');

  constructor(private http: HttpClient) {}

  postLoginDetails(data: IPost) {
    console.log(data);
    return this.http.post(`/api/signin`, data);
  }

  postSignUpDetails(data: IPost) {
    console.log(data);
    return this.http.post(`/api/signUp`, data);
  }

  postForgotDetails(data: any) {
    return this.http.post(`/api/forgot`, data);
  }

  LoggedIn() {
    if (localStorage.getItem('token')) {
      return true;
    } else {
      return false;
    }
  }

  postOTPDetails(data: any) {
    return this.http.post(`/api/verifyOtp`, data);
  }
}
