import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminHomeComponent } from './components/admin-home/admin-home.component';
import { SharedModule } from '../shared/shared.module';
import { MatToolbarModule } from '@angular/material/toolbar';
import { ListComponent } from './components/admin-home/list/list.component';
import { MatTab, MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { HeadersInterceptor } from 'src/app/Interceptors/headers.interceptor';
import { MatButtonModule } from '@angular/material/button';
import { RequestsComponent } from './components/admin-home/requests/requests.component';
import { ProductsComponent } from './components/products/products.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { AdminleaderboardComponent } from './components/adminleaderboard/adminleaderboard.component';



@NgModule({
  declarations: [
    AdminHomeComponent,
    ListComponent,
    RequestsComponent,
    ProductsComponent,
    AdminleaderboardComponent,
  
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    SharedModule,
    MatToolbarModule,
    MatTabsModule,
    MatCardModule,
    MatButtonModule,
    MatSnackBarModule,
    MatSelectModule,
    MatFormFieldModule,
    // MatSelectFilterModule 
  
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: HeadersInterceptor,
    multi: true
}],
})
export class AdminModule { }

