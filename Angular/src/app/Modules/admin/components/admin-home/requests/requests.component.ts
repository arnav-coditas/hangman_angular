import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LeaderboardSectionComponent } from 'src/app/Modules/player/components/profile/leaderboard-section/leaderboard-section.component';
import { DialogComponent } from 'src/app/Modules/shared/Components/dialog/dialog.component';
import { AdminService } from '../../../Services/admin.service';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.scss']
})
export class RequestsComponent implements OnInit {

  constructor( private adminData: AdminService,public dialog: MatDialog,private snakbar:MatSnackBar) { }
  title = 'Arnav';
  subTitle = 'Categories';
  caption = 'Ranks';
  cards = ['card1'];
  items = ['item', 'item2', 'item3', 'item4'];
allRequests:any
  ngOnInit(): void {
    this.getAllTournamentRequests()
    this.getGiftRequests()
  }


getAllTournamentRequests(){
  this.adminData.getRequests().subscribe({
    next: (response) => {
      console.log(response)
      this.allRequests = response
      console.log('Fetching Complete')
      next: () => {}
    },

    complete: () => {

    },
  });
}



  approve(id:number){
    console.log(id)
    const dialogRef = this.dialog.open(DialogComponent,{
      data:{
        description: 'Are you sure to proceed?',
        
      }
    });
   
    dialogRef
      .afterClosed()
      .subscribe((data) => this.onApprove(data,id));
    

  }


  onApprove(data:string,id:any){

   let  approveRequest={
      "tournamentId":id,
      "status":"APPROVED"
  
    }
    if(data==='true'){

     this.adminData.AcceptRequest(approveRequest).subscribe({
      next: (response: any) => {
        console.log('Accept request Complete');
        this.snakbar.open(" Tournament Request Updated Successfully!")
        next: () => {};
      },

      complete: () => {
        setTimeout(() => {
          this.ngOnInit();
        }, 300);
      },
    });

    }
    else{
    console.log("false")
    }
  }

  getGifts:any
getGiftRequests(){
  this.adminData.getGiftRequest().subscribe({
    next: (response) => {
      console.log(response)
      this.getGifts = response
      console.log('Fetching Complete')
      next: () => {}
    },

    complete: () => {
    },
  });
}
acceptGift(id:number){



  console.log(id)
  const dialogRef = this.dialog.open(DialogComponent,{
    data:{
      description: 'Are you sure to proceed?',
      
    }
  });
 
  dialogRef
    .afterClosed()
    .subscribe((data) => this.onGiftApprove(data,id));
  

}

onGiftApprove(data:string,id:number){
  let giftApproved={
  
    "redeemRequestId": id,
    "status":"APPROVED"
  }
  console.log(giftApproved)
  if(data==='true'){
  this.adminData.acceptGiftRequest(giftApproved).subscribe({
    next: (response:any) => {
      console.log(response)
      this.getGifts = response
      console.log('Update Complete')
      this.snakbar.open("ACCEPTED THE REQUEST")
      next: () => {}
    },

    complete: () => {
    },
  });
}
else{
  console.log("Closed")
}
}


rejectGift(id:number){



  console.log(id)
  const dialogRef = this.dialog.open(DialogComponent,{
    data:{
      description: 'Are you sure to proceed?',
      
    }
  });
 
  dialogRef
    .afterClosed()
    .subscribe((data) => this.onGiftReject(data,id));
  

}

onGiftReject(data:string,id:number){
  let giftRejected={
  
    "redeemRequestId": id,
    "status":"REJECTED"
  }
  if(data==='true'){
  this.adminData.acceptGiftRequest(giftRejected).subscribe({
    next: (response:any) => {
      console.log(response)
      this.getGifts = response
      this.snakbar.open("Tournament Rejected Successfully!")
      next: () => {}
    },

    complete: () => {
    },
  });
}
else{
  console.log("Closed")
}
}


rejected(id:number){



  console.log(id)
  const dialogRef = this.dialog.open(DialogComponent,{
    data:{
      description: 'Are you sure to proceed?',
      
    }
  });
 
  dialogRef
    .afterClosed()
    .subscribe((data) => this.onReject(data,id));
  

}


onReject(data:string,id:number){
 let  rejectRequest={
    "tournamentId":id,
    "status":"APPROVED"

  }
  if(data==='true'){
   this.adminData.AcceptRequest(rejectRequest).subscribe({
    next: (response: any) => {
      console.log('Accept request Complete');
      next: () => {};
    },

    complete: () => {
      setTimeout(() => {
        this.ngOnInit();
      }, 300);
    },
  });

  }
  else{
  console.log("false")
  this.snakbar.open("Cancelled")
  }
}



}





  

