import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.scss']
})
export class AdminHomeComponent implements OnInit {



  // items=['Player','Requests','Categories','Products']
  selectedItem!:string

 title= ['Admin']
navtitle='Admin'
     items = [
        // {
        //   tabs :'Player',
        //   icons: 'supervised_user_circle',
        // },
        {
          tabs: 'Requests',
          icons: 'speaker_notes',
        },
        {
          tabs :'Categories',
          icons: 'list_alt',
        },
        {
          tabs: 'Products',
          icons: 'toc',
        },
        {
          tabs: 'LeaderBoard',
          icons: 'list_alt',
        },
        {
          tabs: 'Logout',
          icons: 'toc',
        },
       
      ];
 
  constructor(private router:Router) { }

  ngOnInit(): void {
  }
  onSelectedItem(item:string){
    this.selectedItem=item  
  console.log(this.selectedItem)
  if (this.selectedItem==='Categories'){
    this.router.navigate(['/admin/list'])
  }
  else if(this.selectedItem==='Products'){
    this.router.navigate(['/admin/products'])
  }
  else if(this.selectedItem==='LeaderBoard'){
    this.router.navigate(['/admin/leaderboard'])
  }
  

  else if(this.selectedItem==='Logout'){
    this.router.navigate([''])
  }
  
  else if(this.selectedItem==='Requests'){
    this.router.navigate(['/admin/requests'])
    this.ngOnInit()
  }
}



}
