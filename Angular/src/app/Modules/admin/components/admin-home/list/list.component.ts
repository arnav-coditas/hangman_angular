import { ThisReceiver } from '@angular/compiler';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { an } from 'chart.js/dist/chunks/helpers.core';
import { IPost } from 'src/app/Modules/auth/Interfaces/ipost';
import { UserlistService } from 'src/app/Modules/player/components/playerhome/tournament-list/alltournaments/userlist.service';
import { CreateTournamentDialogComponent } from 'src/app/Modules/shared/Components/create-tournament-dialog/create-tournament-dialog.component';
import { DialogComponent } from 'src/app/Modules/shared/Components/dialog/dialog.component';
import { IDetails } from '../../../idetails';
import { AdminService } from '../../../Services/admin.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  tableData!: any[];
  dataSource!: any[];
  @Input() Event!: any[];
  constructor(
    public router: Router,
    public dialog: MatDialog,
    public admin: AdminService,
    private formBuilder: FormBuilder,
    private snakbar:MatSnackBar
  ) {}

  buttons = ['Edit','Delete'];
  title = 'Arnav';
  subTitle = 'Categories';
  caption = 'Ranks';
  cards = ['card1'];
  items = ['item', 'item2', 'item3', 'item4'];
  ngOnInit(): void {
    this.CategoryDetails();
    this.getAllPlayers();
    this.CategoryDetails();
    this.fetchDetails();




    
  }

  editCategory!: FormGroup;
  createCategoryDetails!: FormGroup;
  editData!: any;
  viewdetails(e: Event) {
    console.log(e);
    this.editData = e;
    this.editCategory.controls['categoryName'].setValue(
      this.editData[0].event.categoryName
    );

    console.log(this.editData[0].event.categoryId);
    if (this.editData[0].details === 'Edit') {
      console.log('open edit dialog');

      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.data = {
        description: 'Edit Cateory',
        type: ['text'],
        placeHolder: ['categoryName'],
        controls: ['categoryName'],
        formgroup: this.editCategory,
        value: this.editData[0].event.categoryName,
      };

      const dialogRef = this.dialog.open(DialogComponent, dialogConfig);
      dialogRef
        .afterClosed()
        .subscribe((data) => this.sendEditedCategory(data));
    } else if (this.editData[0].details === 'Delete') {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.data = {
        description: 'Are you Sure you want to delete?',

        // formgroup: this.deletDoctor,
      };
      const dialogRef = this.dialog.open(DialogComponent, dialogConfig);

      dialogRef.afterClosed().subscribe((data) => this.deleteCategory(data));
    } else {
    }
  }

  sendEditedCategory(data: string) {
    this.admin.editCategoryData(data, this.editData[0].event.categoryId)
      .subscribe({
        next: (response: any) => {
          console.log('post Complete');
          next: () => {};
        },

        complete: () => {
          setTimeout(() => {
            this.ngOnInit();
          }, 300);
        },
      });
  }

  fetchDetails() {
    this.admin.getCategoryDetails().subscribe((res: any) => {
      this.dataSource = res;
      console.log(res);
    });
  }

  CategoryDetails() {
    this.editCategory = this.formBuilder.group({
      categoryName: ['', Validators.required],
    });

    this.createCategoryDetails = this.formBuilder.group({
      categoryName: ['', Validators.required],
    });
  }

  columnHeader = {
    categoryId: ' Category ID',
    categoryName: 'User Name',
    // tournamentList: 'authorEmail',
    // price:'price',
    actions: 'Actions',
  };
  name = 'World';

  openDialog() {
    const dialogConfig = new MatDialogConfig();
    this.dialog.open(CreateTournamentDialogComponent, {
      width: '',
      data: { callback: this.callBack.bind(this), defaultValue: this.name },
    });
  }

  callBack(name: string) {
    this.name = name;
  }

  deleteCategory(data: string) {
    console.log(data);
    if (data === 'true') {
      console.log(this.editData[0].event.categoryId);
      this.admin.deleteCategory(this.editData[0].event.categoryId).subscribe({
        next: (response: any) => {
          next: () => {};
        },

        complete: () => {
          this.snakbar.open("Category Successfully!")
          setTimeout(() => {
            this.ngOnInit();
          }, 300);
        },
      });
    } else {
      console.log('closed');
    }
  }

  createCategory() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      description: 'Create Cateory',
      type: ['text'],
      placeHolder: ['categoryName'],
      controls: ['categoryName'],
      formgroup: this.createCategoryDetails,
    };

    const dialogRef = this.dialog.open(DialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((data) => this.oncreateCategory(data));
  }

  oncreateCategory(data: IPost) {
    this.admin.createCategoryDetails(data).subscribe({
      next: (response: any) => {
        console.log('Create Complete');
        this.snakbar.open("Category Created Successfully")
        next: () => {};
      },

      complete: () => {
        setTimeout(() => {
          this.ngOnInit();
        }, 300);
      },
    });
  }

  allPlayers: any;
  getAllPlayers() {
    this.admin.getAllPlayers().subscribe({
      next: (response: any) => {
        this.allPlayers = response;
        console.log('Fetching Complete');
        next: () => {};
      },

      complete: () => {
  
      },
    });
  }
  
}
