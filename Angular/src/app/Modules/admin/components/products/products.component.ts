import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { IPost } from 'src/app/Modules/auth/Interfaces/ipost';
import { DialogComponent } from 'src/app/Modules/shared/Components/dialog/dialog.component';
import { AdminService } from '../../Services/admin.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
})
export class ProductsComponent implements OnInit {
  tableData!: any[];
  dataSource!: any[];
  @Input() Event!: any[];
  constructor(
    public router: Router,
    public dialog: MatDialog,
    public admin: AdminService,
    private formBuilder: FormBuilder,
    private snakbar:MatSnackBar
  ) {}

  columnHeader = {
    giftId: ' Product Id',
    giftName: 'Product Name',
    giftValue: 'Product Value',
    // price:'price',
    actions: 'Actions',
  };
  buttons = ['Edit','Delete'];

  ngOnInit(): void {
    this.getGiftList();
    this.productDetails();
    
  }

  editCategory!: FormGroup;
  createCategoryDetails!: FormGroup;
  editData!: any;

  viewdetails(e: Event) {
    console.log(e);
    this.editData = e;                                       
    this.editCategory.controls['giftName'].setValue(
      this.editData[0].event.giftName
    );
    this.editCategory.controls['giftValue'].setValue(
      this.editData[0].event.giftValue
    );

    // console.log(this.editData[0].event.categoryId);
    if (this.editData[0].details === 'Edit') {
      console.log('open edit dialog');

      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.data = {
        description: 'Edit Product',
        type: ['giftName', 'giftValue'],
        placeHolder: ['giftName', 'giftValue'],
        controls: ['giftName', 'giftValue'],
        formgroup: this.editCategory,
      };

      const dialogRef = this.dialog.open(DialogComponent, dialogConfig);
      dialogRef.afterClosed().subscribe((data) => this.onEditProduct(data));
    } else  if(this.editData[0].details === 'Delete'){

      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.data = {
        description: 'Are you Sure you want to delete?',

        // formgroup: this.deletDoctor,
      };
      const dialogRef = this.dialog.open(DialogComponent, dialogConfig);

      dialogRef.afterClosed().subscribe((data) => this.deleteCategory(data));
    } 


      console.log('Selection Failed');
    }
  

  productDetails() {
    this.editCategory = this.formBuilder.group({
      giftValue: ['', Validators.required],
      giftName: ['', Validators.required],
    });

    this.createCategoryDetails = this.formBuilder.group({
      giftValue: ['', Validators.required],
      giftName: ['', Validators.required],
    });
  }

  getGiftList() {
    this.admin.getAllProducts().subscribe({
      next: (response: any) => {
        this.dataSource = response;
        console.log(response);

        console.log('DisplayingComplete');
        next: () => {};
      },

      complete: () => {},
    });
  }

  deleteCategory(data: string) {
    console.log(data);
    if (data === 'true') {
      console.log(this.editData[0].event.giftId);
      this.admin.deleteProduct(this.editData[0].event.giftId).subscribe({
        next: (response: any) => {
          next: () => {};
        },

        complete: () => {
          this.snakbar.open(` Deleted ${this.editData[0].event.giftName} Successfully!`)
          setTimeout(() => {
            this.ngOnInit();
          }, 300);
        },
      });
    } else {
      console.log('closed');
    }
  }






  createProduct() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      description: 'Create Product',
      type: ['text', 'text'],
      placeHolder: ['giftName', 'giftValue'],
      controls: ['giftName', 'giftValue'],
      formgroup: this.createCategoryDetails,
    };

    const dialogRef = this.dialog.open(DialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((data) => this.oncreateProduct(data));
  }

  oncreateProduct(data: string) {
    console.log(data);
    this.admin.createProducts(data).subscribe({
      next: () => {
        console.log('Create Complete');
        this.snakbar.open("Update Complete")
        next: () => {};
      },

      complete: () => {
        setTimeout(() => {
          this.ngOnInit();
        }, 300);
      },
    });
  }

  onEditProduct(data: any) {

    let updateGift={
      "giftId": this.editData[0].event.giftId,
      "giftValue":data.giftValue

    }
    this.admin.editProduct(updateGift).subscribe({

 
      next: (response: any) => {
       
        next: () => {};
      },

      complete: () => {
        this.snakbar.open("Update Complete")
        setTimeout(() => {
          this.ngOnInit();
        }, 300);
      },
    });
  }
}
