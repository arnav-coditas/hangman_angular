import { Component, OnInit } from '@angular/core';
import { PlayerService } from 'src/app/Modules/player/Services/player.service';
import { AdminService } from '../../Services/admin.service';

@Component({
  selector: 'app-adminleaderboard',
  templateUrl: './adminleaderboard.component.html',
  styleUrls: ['./adminleaderboard.component.scss']
})
export class AdminleaderboardComponent implements OnInit {

  constructor(private adminservice:AdminService,private categoryservice:PlayerService) { }

  ngOnInit(): void {
    this.getCategories()
    this.getLeaderBoardData()
  }

//   page=['0','5','10','15','20']
// sortBy=['asc','desc']

  tableData!: any[];
  buttons = ['Edit','Delete'];
  title = 'Arnav';
  subTitle = 'Categories';
  caption = 'Ranks';
  cards = ['card1'];
  items = ['item', 'item2', 'item3', 'item4'];
  dataSource:any
  columnHeader = {
    tournamentId: 'ID',
    tournamentName: 'Tournament Name',
    score: 'score',
    gameDuration: 'Game Duration',
    playerName: 'Player Name',
    // actions: 'Actions',
  };
// categoryValues:string[]=[]
categoryValues: string='';
categories:any
filtervalue:string[]=[]

page=['3','5','10','15','20']
sortBy=['asc','desc']
order:string[]=['desc']
pageNo:number=3


  getLeaderBoardData(){
    const url = `pageSize=${this.pageNo}&sortBy=score&pageNo=0&sortDir=${this.order}&categoryValues=${this.categoryValues}`
  
    // console.log(url)
    this.adminservice.getAllLeaderBoardList(url).subscribe((res: any) => {
      this.dataSource = res;
    })
  }
  filterByCategory(value:string[]){
    this.categoryValues = value.join(',')
    this.getLeaderBoardData()
  }

  filterByPage(value:number){
    console.log(value)
    this.pageNo=value
    console.log(value)
    this.getLeaderBoardData()
  }
filterBySort(value:string[]){
console.log(value)
this.order=value
this.getLeaderBoardData()

}
  // order:string[]=['desc']
  // pageNo:number=5
  
  getCategories(){
    this.categoryservice.getCategoryDetails().subscribe((res: any) => {
      console.log(res)
     this.categories= res;
    
      }  )
  
  }



}
