import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminleaderboardComponent } from './adminleaderboard.component';

describe('AdminleaderboardComponent', () => {
  let component: AdminleaderboardComponent;
  let fixture: ComponentFixture<AdminleaderboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminleaderboardComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdminleaderboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
