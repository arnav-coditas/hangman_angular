import { leadingComment } from '@angular/compiler';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminHomeComponent } from './components/admin-home/admin-home.component';
import { ListComponent } from './components/admin-home/list/list.component';
import { RequestsComponent } from './components/admin-home/requests/requests.component';
import { AdminleaderboardComponent } from './components/adminleaderboard/adminleaderboard.component';
import { ProductsComponent } from './components/products/products.component';

const routes: Routes =  [{
  path: '',
    component: AdminHomeComponent, children: [
         
      {
        path: 'list',
        component: ListComponent
      },
      {
        path: 'requests',
        component: RequestsComponent
      },
      {
        path: 'products',
        component: ProductsComponent
      },
      {
        path: 'leaderboard',
        component: AdminleaderboardComponent
      },
      
    ]
}];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
