import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IPost } from '../../auth/Interfaces/ipost';
import { IDetails } from '../idetails';

@Injectable({
  providedIn: 'any',
})
export class AdminService {


  constructor(private http: HttpClient) {}

  postLoginDetails(data: IPost) {
    console.log(data);
    // this.uservalue=data
    return this.http.post(`/api/signin`, data);
  }

  getCategoryDetails() {
    return this.http.get('/getCategoryList');
  }

  editCategoryData(data: string, Id: number) {
    return this.http.put(`/api/updateCategory/${Id}`, data);
  }
  deleteCategory(Id:number){
    console.log(Id)
    return this.http.delete(`/deleteCategory/${Id}`)
  }


  createCategoryDetails(data:IPost){
    return this.http.post('/addCategory',data)
  }

  getAllPlayers(){
    return this.http.get('/getPlayers')
  }

getAllProducts(){
  return this.http.get('/gifts/getGiftList')
}

editProduct(data:any){
return this.http.put('/gifts/updateGifts',data)

}


createProducts(data:string){
return this.http.post('/gifts/addGifts',data)
}

getRequests(){
  return this.http.get('/getPendingTournamentRequests')
}

AcceptRequest(data:any){

  return this.http.post(`/approveTournamentRequest`,data)
}

getGiftRequest(){
  return this.http.get(`/request/getGiftRequests`)
}
acceptGiftRequest(data:any){
  return this.http.post(`/request/updateRequest`,data)
}

deleteProduct(id:number){
  return this.http.delete(`/gifts/deleteGift/${id}`)
}

getAllLeaderBoardList(url:any){  
  return this.http.get(`/api/getAllLeaderBoardList?${url}`)
}
}

